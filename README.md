## In this project we are going to deploy the reactjs based boiler code on netlify server using ci/cd .
## Requirements
```
- linux os
- node js should be installed version > 14

```


## Installation steps

- Install the react tool using the following commands  :- `sudo npm -g install create-react-app`
- Create a react  project using the following commands :- `create-react-app my-app`
- change directory to my-app 
- Now we will build our project to manually deploy it in our host server which is a later part so we run the following command to build :- `npm run build`
- Run the following command to see the demo project :- `pm start`
- After that we have to set up the ci/cd pipeline 
- So first we will make the two stages of ci/cd pipeline and below are the following stages:-
  - test
  - build

So we create the .gitlab-ci.yml file inside our project root folder and write the below commands:-
## 
```
test project:
    stage: test
    image: node:18
    script:
      - npm install
      - npm test

build project:
    stage: build
    image: node:18
    script:
      - npm install
      - npm test
      - npm run build
   
```
 
- After this we will make a remote repo and push this code after commits inside that repo and we will see that our jobs are running using ci/cd pipeline.

- So to deploy our project on server we are using netlify free version to host our project.
- So create an account on netlify and after creating it we put our build folder inside the netlify `new site option below right side `.
- Now we have deploy our website manually on netlify and after deployment we get the link of our project above and using that link we can see our hosted website  from any device.
- Know We will deploy our project automatically using ci/cd pipeline so inside .gitlab-ci.yml we will add the third stage i.e. `deploy`.
- so we will edit our .yml file with third stage as below:-
## 
```
stages:
    - test
    - build
    - deploy

test project:
    stage: test
    image: node:18
    script:
      - npm install
      - npm test

build project:
    stage: build
    image: node:18
    script:
      - npm install
      - npm test
      - npm run build
    artifacts:
      paths:`
        - build/

netlify:
    stage: deploy
    image: node:18
    script:
      - npm install -g netlify-cli
      - netlify deploy --dir=build --prod
```     


- After this we need to configure two additional variable inside gitlab ci i.e.

## First variable
- So click the settings then ci/cd & then variable and create our first variable `NETLIFY_AUTH_TOKEN`
- Then we have to write its value,so to get the value we have to go to netlify login page.
- Then go to Account,then user settings,then application and then we need to create a new access token.
- Before generating a token we have to give the description so we write the description as `GitLab CI` .
- Then we will generate the token and we copy this token and paste it in the first variable value and then we will click the add variable button and we have succesfully generated our first variable.

**`NOTE While pasting avoid the newline and new space`
## Second variable
- So click the settings then ci/cd & then variable and create our second variable `NETLIFY_SITE_ID`.
- Then we have to write its value,so to get the value we have to go to netlify login page.
- Then click on the site that we had manually created.
- Then we will go to the site settings and copy the id from the right side properties and paste it inside the second variable value.
- Then we will click the add variable button and we have succesfully generated our second variable.
- So to test the automatic deployment process we will edit our app.js file and edit the paragraph then save it and commit it.
- After that we will push the changes and we will see that our website get automatic deploy with the changes that we have made on the earlier project link.

